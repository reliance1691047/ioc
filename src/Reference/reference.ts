export interface ReferenceInterface {
    getServiceId(): string;
    toString(): string;
}

export class Reference implements ReferenceInterface {
    constructor(
        private serviceId: string
    ) {
    }

    getServiceId(): string {
        return this.serviceId;
    }

    toString(): string {
        return this.getServiceId();
    }
}
