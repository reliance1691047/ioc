# ioc

## Getting started

1. Clone repository
2. Go to docker folder in console
3. run `docker-compose up -d`
4. run `docker exec -it node-dev bash`
5. run `npm install`
6. run `npm run test`
