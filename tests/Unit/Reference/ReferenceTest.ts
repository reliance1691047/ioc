import { suite, test } from '@testdeck/jest';
import { Reference } from "../../../src";
import { faker } from '@faker-js/faker';

@suite
export class ReferenceTest {
    @test
    testReferenceGetServiceId(): void {
        const serviceId = faker.string.uuid()
        const reference = new Reference(serviceId)

        expect(reference.getServiceId()).toEqual(serviceId)
        expect(reference.toString()).toEqual(reference.getServiceId())
        expect(`${reference}`).toEqual(serviceId)
    }
}
