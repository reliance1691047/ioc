module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverage: true,

    collectCoverageFrom: ['src/**/*.[jt]s?(x)', 'src/*.[jt]s?(x)'],

    testMatch: ['<rootDir>/tests/**/*Test.[jt]s?(x)'],

    clearMocks: true,
    moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
};
